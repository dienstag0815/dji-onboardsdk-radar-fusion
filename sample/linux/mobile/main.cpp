#include "mobile_sample.hpp"
#include <arpa/inet.h> //inet_addr
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <chrono>
#include <fstream>  


//#define GPSuse
//#define RADARuse
int Radar1GPS0Variable = 0;
int RadarDown = 1;
int RadarFront = 0;
#define MaxVertTempo 2


#define radar_ip "192.168.0.98"
#define telemUpdateRate 5

#define C_EARTH (double)6378137.0
#define DEG2RAD 0.01745329252

using namespace std;
using namespace std::chrono;
using namespace DJI;
using namespace DJI::OSDK;
using namespace DJI::OSDK::Telemetry;
  
Telemetry::GlobalPosition   globalPosition;
Telemetry::Status           status;
Telemetry::GPSInfo          data;
Telemetry::Vector3f         velocity;
Telemetry::Quaternion       quaternion;
    


short tx_msg;
unsigned int tx_msg_return;
int msg_size;
short echo;
int socket_desc;

int functionTimeout = 1;
int enableRadar;
//variables for loop-functions
int startRadarMeasurement=0;
int sendTelemetry=1;
//GoToHeightModus
int gotoHeightStart=0;
float reachedHeigthMin;
float reachedHeigthMax;
long info; 
//Limit Modus
int limitHeightStart=0;
int limitFrontStart=0;
long limitMinDown;
long limitMaxDown;
long limitMinFront;
long limitMaxFront;
//Tracking
float distanceRadar;
float distanceTrackVariance;
//
int OSDKControl=0;
//Logging
int LoggingRadar=1;
int LoggingTelemetry=1;
//
int StatusCounter;
//heightcalibration
float calibHeigthBaro;

std::string Header;
string s3String;
string s4String;
string s5String;
string radarRecordingFileName = "radarRecording.txt";
string telemetryRecordingFileName = "telemetryRecording.txt";
int batCap,batCap_;

struct sockaddr_in server;
struct radardata 
{
    float rf_distance;
    float rf_angle_deg;
    signed int rf_speed;
    float rf_time;  
  };

typedef struct CtrlData
  {
    uint8_t flag;  /*!< control data flag consists of 8 bits.
 
                      - CtrlData.flag = ( DJI::OSDK::Control::HorizontalLogic |
                      DJI::OSDK::Control::VerticalLogic |
                      DJI::OSDK::Control::YawLogic |
                      DJI::OSDK::Control::HorizontalCoordinate |
                      DJI::OSDK::Control::StableMode)
                   */
    float32_t x;   /*!< Control with respect to the x axis of the
                      DJI::OSDK::Control::HorizontalCoordinate.*/
    float32_t y;   /*!< Control with respect to the y axis of the
                      DJI::OSDK::Control::HorizontalCoordinate.*/
    float32_t z;   /*!< Control with respect to the z axis, up is positive. */
    float32_t yaw; /*!< Yaw position/velocity control w.r.t. the ground frame.*/

    /*!
     * \brief CtrlData initialize the CtrlData variable.
     * \param in_flag   See CtrlData.flag
     * \param in_x      See CtrlData.x
     * \param in_y      See CtrlData.y
     * \param in_z      See CtrlData.z
     * \param in_yaw    See CtrlData.yaw
     */
    CtrlData(uint8_t in_flag, float32_t in_x, float32_t in_y, float32_t in_z,
             float32_t in_yaw);
  } CtrlData; 

///Declerations
int init_rf_measurement(void);
struct radardata do_rf_measurement(int measurement_count);
void obtainOSDKControl(Vehicle* vehicle);
void releaseOSDKControl(Vehicle* vehicle);
void sendStringStreamToMSDK(std::stringstream& strIn, Vehicle* vehicle);
void sendStringStreamStatusToMSDK(std::stringstream& strIn, Vehicle* vehicle);
void sendStringStartStatusToMSDK(Vehicle* vehicle);
void sendStringStatusErrorToMSDK(std::stringstream& strIn, Vehicle* vehicle);
void updateTelemetryData(Vehicle* vehicle);
void emergencyBrake_diy(Vehicle* vehicle);

//Create File for Writing Radar
std::ofstream ofs;

int main(int argc, char** argv)
{
    auto starttimemain = std::chrono::high_resolution_clock::now();

    //// Initialize variables 
    int telemCounter;
    struct radardata r;



    // Setup OSDK.
    LinuxSetup linuxEnvironment(argc, argv);
    Vehicle*   vehicle = linuxEnvironment.getVehicle();
    if (vehicle == NULL)
    {
        cout << "Vehicle not initialized, exiting.\n";
        return -1;
    }

    cout << "Radar Installed? 1 = y, 0 = n"<< endl;
    char inputChar = ' ';
    std::cin >> inputChar;

    switch (inputChar)
    {
    case '1':
     //up Socket & Radar
    if(init_rf_measurement() == 0)
    {
        stringstream st1;
        st1 << "Cant connect to Radar - continue without Radar or Restart\n ";
        sendStringStatusErrorToMSDK(st1, vehicle);   
        
        startRadarMeasurement=0;
        Radar1GPS0Variable = 0;
        enableRadar=0;
        //return -1;
    }
    else
    {
        stringstream st1;
        st1 << "Radar detected and socket created \n";
        sendStringStreamStatusToMSDK(st1, vehicle);
        enableRadar=1;
        //Radar1GPS0Variable = 1;
    }
        break;
    case '0':
            stringstream st1;
        st1 << "Cant connect to Radar - continue without Radar or Restart\n ";
        sendStringStatusErrorToMSDK(st1, vehicle);   
        
        startRadarMeasurement=0;
        Radar1GPS0Variable = 0;
        enableRadar=0;
        //return -1;
        break;
  }
    
//   //up Socket & Radar
//     if(init_rf_measurement() == 0)
//     {
//         stringstream st1;
//         st1 << "Cant connect to Radar - continue without Radar or Restart\n ";
//         sendStringStatusErrorToMSDK(st1, vehicle);   
        
//         startRadarMeasurement=0;
//         Radar1GPS0Variable = 0;
//         enableRadar=0;
//         //return -1;
//     }
//     else
//     {
//         stringstream st1;
//         st1 << "Radar detected and socket created \n";
//         sendStringStreamStatusToMSDK(st1, vehicle);
//         enableRadar=1;
//         //Radar1GPS0Variable = 1;
//     }

    if(Radar1GPS0Variable==0)
    {
        stringstream st1;
        st1 << "Working in GPS Mode\n";
        sendStringStreamStatusToMSDK(st1, vehicle);
    }
    else
    {
        stringstream st1;
        st1 << "in Radar Mode, working with Radar altitude \n";
        sendStringStreamStatusToMSDK(st1, vehicle);
    }

    //Check OSDK Control on/off
    obtainOSDKControl(vehicle);
    releaseOSDKControl(vehicle);
    //send StartStatus of the app
    //Status of Radar / Telemetry / GPS / Logging
    sendStringStartStatusToMSDK(vehicle);


    cout << "Logging? 1 = y, 0 = n"<< endl;
    char inputCharL = ' ';
    std::cin >> inputCharL;
    
    switch (inputCharL)
    {
        case '1':
        LoggingRadar = 1;
        LoggingTelemetry = 1;
            break;
        case '0':
        LoggingRadar = 0;
        LoggingTelemetry = 0;
            break;  
    }

    stringstream mainLoopStatus;
    mainLoopStatus 
    << "Main Loop Started with Radar: " << startRadarMeasurement << endl
    << "Log Radar " << LoggingRadar << " Log Telemetry " << LoggingTelemetry << endl;
    sendStringStreamStatusToMSDK(mainLoopStatus, vehicle);

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

    puts("loop starts\n");
    while(1)
    {            
        // Record start time
        auto starttime = std::chrono::high_resolution_clock::now();

        //Receive Data
        vehicle->moc->setFromMSDKCallback(parseFromMobileCallback, &linuxEnvironment);
        auto afterReceive = std::chrono::high_resolution_clock::now();

        //Update Telemetry Date
        updateTelemetryData(vehicle);
        auto afterTelemetry = std::chrono::high_resolution_clock::now();

        //Zero flight level before take off
        if(!status.flight)
        { 
            if(globalPosition.altitude < 0)
            {
                calibHeigthBaro = -1*globalPosition.altitude;
            }
        }
        //Radar Measurement
        if(startRadarMeasurement&&enableRadar)
        {
            //do 1 Measurement in Range Finder Mode and put Data in r struct with rf_distance, rf_angle_deg and rf_speed
            r = do_rf_measurement(1);
            
            if(r.rf_distance > 300)
            {r.rf_distance=0;}
            //Add 100degree to stay positive
            //r.rf_angle_deg = r.rf_angle_deg+100;


            //Put String together to Send
            std:stringstream s;
            s  << "R," << std::fixed << std::setprecision(1) << +r.rf_distance << "," 
                << std::fixed << std::setprecision(1) << +r.rf_angle_deg+100 << ","
                << std::dec << +r.rf_speed 
            << endl;
            sendStringStreamToMSDK(s, vehicle);


            if(LoggingRadar)
            {
                //Writing to File
                stringstream stream_rftime;
                stream_rftime  << "R," << fixed << r.rf_time << std::fixed << std::setprecision(1) << +r.rf_distance << "," 
                    << std::fixed << std::setprecision(1) << +r.rf_angle_deg << ","
                    << std::dec << +r.rf_speed 
                << endl;

                string string_rftime = stream_rftime.str();
                //cout << "Writing to file: " << string_rftime;           
                if (!ofs) 
                { 
                    std::cerr<<"Error writing to ..."<<radarRecordingFileName<<std::endl; 
                    } 
                else 
                {
                    ofs.open (radarRecordingFileName, std::ofstream::out | std::ofstream::app);
                    ofs << string_rftime; 
                    ofs.close(); 
                    }  
            }

        }        
        auto afterRadar = std::chrono::high_resolution_clock::now();
   
        //Goto Heigth Function
        if(gotoHeightStart)
        {
            float calcHeigth;
            //Needs Radar facing Down and Radar active instead of GPS, oterwise it will use GPS to maintain Height
            if(Radar1GPS0Variable && RadarDown && startRadarMeasurement)
                {
                    calcHeigth = r.rf_distance;
                    }

            else if(Radar1GPS0Variable==0)
            {
                calcHeigth = globalPosition.altitude;
                }

            //go to defined heigth
            //velocity = tempo in m/s up to 5 m/s (x,y,z,yaw)
            if(globalPosition.altitude ==0 || status.flight ==0 )
            {
                stringstream stx;              
                stx << "Data not Valid or Vehicle is still on the Ground" << endl;
                sendStringStreamStatusToMSDK(stx, vehicle);
                return 0;
                
                    }


            //slow down for last 4m
            //info = destination
            if(abs(info-calcHeigth)<4)
            {
                //slow down for last 4m
                if(info>calcHeigth)
                {
                    vehicle->control->velocityAndYawRateCtrl(0,0,0.5,0);
                    stringstream stx;              
                    stx << "Going Up - heigth: "<< calcHeigth << endl;
                    sendStringStreamStatusToMSDK(stx, vehicle);
                }
                if(info<calcHeigth)
                {
                    vehicle->control->velocityAndYawRateCtrl(0,0,-0.5,0);
                    stringstream stx;              
                    stx << "Going down - heigth: "<< calcHeigth << endl;
                    sendStringStreamStatusToMSDK(stx, vehicle);
                    }
            }   
            else
            {        
                if(info>calcHeigth)
                {
                    vehicle->control->velocityAndYawRateCtrl(0,0,MaxVertTempo,0);
                    stringstream stx;              
                    stx << "Going Up - heigth: "<< calcHeigth << endl;
                    sendStringStreamStatusToMSDK(stx, vehicle);
                    }   
                if(info<calcHeigth)
                {
                    vehicle->control->velocityAndYawRateCtrl(0,0,-MaxVertTempo,0);
                    stringstream stx;              
                    stx << "Going down - heigth: "<< calcHeigth << endl;
                    sendStringStreamStatusToMSDK(stx, vehicle);
                    }

            }
            
            if((calcHeigth > reachedHeigthMin)  && (calcHeigth < reachedHeigthMax))
            {
                gotoHeightStart=0;
                releaseOSDKControl(vehicle);
                //std::cout <<"Height Min: " << reachedHeigthMin << endl;        
                stringstream sty;              
                sty << "OSDK Control Released -" << " Height Reached: " << calcHeigth << endl;
                sendStringStreamStatusToMSDK(sty, vehicle);             
                }
        } 
        auto afterGoTo = std::chrono::high_resolution_clock::now();

        //Limit Front Distance
        if(limitFrontStart)
        {
            float calcDistance;

            //Check if Radar is installed and connected,otherwise disconnect
            if(RadarFront && startRadarMeasurement)
                calcDistance = r.rf_distance;
            else
               {                  
                    stringstream stz;
                    stz << "ERROR: No Front-Radar installed or connected" << endl;   
                    sendStringStreamStatusToMSDK(stz, vehicle);  
                    limitFrontStart=0;
                    return 0;
               }

            //If an Object is detected  inside the Limit, 
            // Vehicle will stop and move slowly backwards until the distance is reached
            //1 Take Control and Stop vehicle           
            if(calcDistance <= limitMinFront || calcDistance >= limitMaxFront)
            {   
                //Take Control and Stop vehicle
                if(OSDKControl==0)
                {
                    obtainOSDKControl(vehicle);
                    emergencyBrake_diy(vehicle);
                    stringstream stz;
                    stz << "too close to distance limit - vehicle stopped" << endl;   
                    sendStringStreamStatusToMSDK(stz, vehicle);           
                    }
                
                //decide which limit is in danger and move away
                if(calcDistance <= limitMinFront)
                {                                  
                    // void velocityAndYawRateCtrl_Body(float32_t Vx, float32_t Vy, float32_t Vz,
                    //           float32_t yawRate);
                    //Pitch, Roll, Altitude, YawRate
                    vehicle->control->velocityAndYawRateCtrl_Body(-0.5,0,0,0);
                    
                    
                    stringstream stx;              
                    stx << "too close- backing up - distance: "<< calcDistance << endl;
                    sendStringStreamStatusToMSDK(stx, vehicle);

                    }

                
                //vehicle will close gap if distance is too far
                else if(calcDistance >= limitMaxFront)
                {
                    // vehicle will slowly accelerate
                    //vehicle->control->angularRateAndVertPosCtrl(0,-1,0,globalPosition.altitude);
                    vehicle->control->velocityAndYawRateCtrl_Body(0.5,0,0,0);
                    stringstream stx;              
                    stx << "too far away - coming closer - distance: "<< calcDistance << endl;
                    sendStringStreamStatusToMSDK(stx, vehicle);
                    }
                    
            } else
            {
                if(OSDKControl==1)
                {  
                    releaseOSDKControl(vehicle);                                           
                    stringstream sti;
                    sti << "vehicle is save - keep flying" << endl;   
                    sendStringStreamStatusToMSDK(sti, vehicle);     
                    OSDKControl=0;                   
                    }
            }

        } 
        auto afterTrackingFront = std::chrono::high_resolution_clock::now();

        //Limit Down Distance
        if(limitHeightStart && gotoHeightStart==0)
        {
            float calcDistance;
            
            if(Radar1GPS0Variable && RadarDown)
                calcDistance = r.rf_distance;
            else
                calcDistance = globalPosition.altitude;
                
            
            //go to defined heigth
            //velocity = tempo in m/s up to 5 m/s (x,y,z,yaw)
            //Act if Vehicle is too close to limit
            if( calcDistance <= limitMinDown || calcDistance >= limitMaxDown)
            {   
                //Take Control and Stop vehicle
                if(OSDKControl==0)
                {
                    obtainOSDKControl(vehicle);
                    emergencyBrake_diy(vehicle);

                    stringstream stz;
                    stz << "too close to distance limit - vehicle stopped" << endl;   
                    sendStringStreamStatusToMSDK(stz, vehicle);           
                    }
                //decide which limit is in danger and move away
                //if min limit: 10-9 or 10-11 gain distance to atleast 11m
                // 10-13 =3
                if(calcDistance <= limitMinDown)
                {
                    
                    vehicle->control->velocityAndYawRateCtrl(0,0,MaxVertTempo,0);
                    stringstream stx;              
                    stx << "too low- going up - heigth: "<< calcDistance << endl;
                    sendStringStreamStatusToMSDK(stx, vehicle);

                    }

                //if max limit: 50 - 49 or 50 - 51 loose altitude to atleast 49m
                else if(calcDistance >= limitMaxDown)
                {
                    vehicle->control->velocityAndYawRateCtrl(0,0,-MaxVertTempo,0);
                    stringstream stx;              
                    stx << "too high- going down - heigth: "<< calcDistance << endl;
                    sendStringStreamStatusToMSDK(stx, vehicle);
                    }
                    
            } else
            {
                if(OSDKControl==1)
                {   auto startreleaseAuthority = std::chrono::high_resolution_clock::now();
                    releaseOSDKControl(vehicle);
                    auto endAuthority = std::chrono::high_resolution_clock::now();
                    
                    chrono::duration<double> elapsedReleaseAuthority = endAuthority - startreleaseAuthority; //10 - 1

                    stringstream sti;
                    sti << "vehicle is save - keep flying" << endl;   
                    sendStringStreamStatusToMSDK(sti, vehicle);     
                    OSDKControl=0;
                    //limitHeightStart=0;
                    }
            }
            
        } 
        auto afterLimitFinish = std::chrono::high_resolution_clock::now();


        //Time Measurements
        chrono::duration<double> elapsedComplete = afterLimitFinish - starttime; //10 - 1
        chrono::duration<double> elapsedReceive = afterReceive - starttime; // 2 - 1
        chrono::duration<double> elapsedTelemetry = afterTelemetry - afterReceive; //3 - 2
        chrono::duration<double> elapsedRadar = afterRadar - afterTelemetry ;//4 -3
        chrono::duration<double> elapsedGoTo = afterGoTo - afterRadar; //5 -4
        chrono::duration<double> elapsedLimitFinish = afterLimitFinish - afterGoTo; //6 -5

    ////////////////////////////Time Prints///////////////////////////////////////////////
        //cout << "Receive time: " << elapsedReceive.count()*1000 << " ms\n";
        //cout << "Telemetry time: " << elapsedTelemetry.count()*1000 << " ms\n";
        //cout << "Radar time: " << elapsedRadar.count()*1000 << " ms\n";
        //cout << "GoTo Heigth time: " << elapsedGoTo.count()*1000 << " ms\n";
        //cout << "Limit Heigth time: " << elapsedLimitFinish.count()*1000 << " ms\n";
        //cout << "Elapsed Complete time: " << elapsedComplete.count()*1000 << " ms\n";
    ///////////////////////////////////////////////////////////////////////////

        int waitingTime = 50-(elapsedComplete.count()*1000);
        if(waitingTime < 50 && waitingTime > 0)
        {
            usleep(waitingTime*1000);
            }
        
        //Sends StatusUpdates every 50*50ms = 2.5s
        StatusCounter++;
        if(StatusCounter==50)
        {
            sendStringStartStatusToMSDK(vehicle);
            StatusCounter=0;

            stringstream stRGi;              
            stringstream stRGu;    
            stringstream stRGq; 
            stRGi << "Global Position Altitude: " << (float)globalPosition.altitude << " fixed: " << calibHeigthBaro+globalPosition.altitude << endl;
            stRGu << "Velocity(vx-vy-vz)      : " 
                    << fixed << setprecision(3) <<  velocity.x   
            << "/ " << fixed << setprecision(3) <<  velocity.y 
            << "/ " << fixed << setprecision(3) <<  velocity.z << "\n";
                        
            sendStringStreamStatusToMSDK(stRGi, vehicle);     
            sendStringStreamStatusToMSDK(stRGu, vehicle);     
            
        }
            //
            // cout << "Velocity(vx-vy-vz)      : " 
            //         << fixed << setprecision(3) <<  velocity.x   
            // << "/ " << fixed << setprecision(3) <<  velocity.y 
            // << "/ " << fixed << setprecision(3) <<  velocity.z << "\n";
        
    }

    //close socket
    printf("-end ");
    close(socket_desc);
    printf("-socket closed \n");
        
    //Close OSDK
    delete (vehicle);
    return 0;

    // // Record end time
    // auto finish = std::chrono::high_resolution_clock::now();
    // std::chrono::duration<double> elapsed = finish - start;
    // std::cout << "Elapsed time: " << elapsed.count() << " s\n";

    // Record start time
    //auto start = std::chrono::high_resolution_clock::now();
}

void
parseFromMobileCallback(Vehicle* vehicle, RecvContainer recvFrame,
                        UserData userData)
{
    // First, lets cast the userData to LinuxSetup*
    LinuxSetup* linuxEnvironment = (LinuxSetup*)userData;
    int sizeofFrame = sizeof(recvFrame.recvData.raw_ack_array);                                                                                                                  char rx_char_array[sizeofFrame];
    memcpy(rx_char_array,recvFrame.recvData.raw_ack_array,sizeofFrame);
    //Incoming String = rx_char_array
    //split by \n to find end of string
    char *token = strtok(rx_char_array,"\n");
    std:stringstream s1;
    s1 << token;
    string stringData = s1.str();

    //cout << "RX: " << token << endl;

    //split second time to have acces to input data from mobile
    //transform String to Char array for split token function
    char char_array[stringData.length()];
    strcpy(char_array, stringData.c_str()); 
    //split
    char *tokenData = strtok(char_array,",");
    //check if numeric data is available after command
    stringstream s2;
    stringstream s3;
    stringstream s4;
    stringstream s5;
    s2 << tokenData;
    if(tokenData != NULL)
    {
        //S2 = Header
        Header = s2.str();
        tokenData = strtok(NULL, ",");
        s3 << tokenData;
        s3String = s3.str();
        tokenData = strtok(NULL, ",");
        s4 << tokenData;
        s4String = s4.str();
        tokenData = strtok(NULL, ",");
        s5 << tokenData;
        s5String = s5.str();

        //  cout  << "s2: " << Header << "-" << endl;
        //  cout  << "s3: " << s3String << "-" << endl;
        //  cout  << "s4: " << s4String << "-" << endl;
        //  cout  << "s5: " << s4String << "-" << endl;
    }

    /////////////////////START RADAR
    if(Header.compare("StartRadar") ==0)
    {
        info = std::strtol(s3String.c_str(), 0, 10);
        if(info==1&&enableRadar)
        {
            startRadarMeasurement=1;

            stringstream st;              
            st << "Start Radar"<< endl;
            sendStringStreamStatusToMSDK(st, vehicle);    
            sendStringStartStatusToMSDK(vehicle); 
            if(LoggingRadar)
            {
                ofs.open (radarRecordingFileName, std::ofstream::out | std::ofstream::app);
                ofs << "Start RadarMeasurement" << endl << "time,distance,angle,speed,"<<endl; 
                ofs.close(); 
            }

            if(LoggingTelemetry)
            {
                ofs.open (telemetryRecordingFileName, std::ofstream::out | std::ofstream::app);
                ofs << "Start Telemetry\n" << "time,flightstatus,battery,altitude,heigth,gpsstatus" << endl; 
                ofs.close(); 
            }
        }
        else 
        {
            startRadarMeasurement=0;

            stringstream st;              
            st << "Stop Radar"<< endl;
            sendStringStreamStatusToMSDK(st, vehicle);
            cout << st.str();
            
            sendStringStartStatusToMSDK(vehicle);
            if(LoggingRadar)
            {
                ofs.open (radarRecordingFileName, std::ofstream::out | std::ofstream::app);
                ofs << "Stop RadarMeasurement"<< endl;
                ofs.close(); 
            }

            if(LoggingTelemetry)
            {
                ofs.open (telemetryRecordingFileName, std::ofstream::out | std::ofstream::app);
                ofs << "Stop Telemetry\n"; 
                ofs.close(); 
            }

            }
        
    }

    
    /////////////////////USE RADAR OR GPS
    else if(Header.compare("UseRadar") ==0 )
    {
        info = std::strtol(s3String.c_str(), 0, 10);
        if(gotoHeightStart==0 && limitHeightStart==0)
        {
            if(info==1)
            {
                
                Radar1GPS0Variable=1;
                stringstream st;              
                st << "Use Radar"<< endl;
                sendStringStreamStatusToMSDK(st, vehicle);
                sendStringStartStatusToMSDK(vehicle);
                
                }
            else 
            {
                Radar1GPS0Variable=0;
                stringstream st;              
                st << "Use GPS"<< endl;
                sendStringStreamStatusToMSDK(st, vehicle);
                sendStringStartStatusToMSDK(vehicle);
                }
       } else 
       {
                stringstream st;              
                st << "Cant change measurement mode while Flying a Maneuver"<< endl;
                sendStringStatusErrorToMSDK(st, vehicle);

            }
      }
//////////////////Radar Down or Front
    else if(Header.compare("RadarInstalled") ==0 )
    {
        if(stringData.compare("RadarInstalled,Front,") ==0)
            {
                
                RadarFront=1;
                RadarDown=0;
                stringstream st;              
                st << "RadarFront enabled"<< endl;
                sendStringStreamStatusToMSDK(st, vehicle);
                sendStringStartStatusToMSDK(vehicle);
                
                }
    if(stringData.compare("RadarInstalled,Down,") ==0)
            {
                RadarDown=1;
                RadarFront=0;
                stringstream st;              
                st << "RadarDown enabled"<< endl;
                sendStringStreamStatusToMSDK(st, vehicle);
                sendStringStartStatusToMSDK(vehicle);
                }
       
    }
    /////////////////////GOTO
    else if(Header.compare("GoTo") ==0)
    {
         
        if(status.flight)          
        {
            if(Radar1GPS0Variable && startRadarMeasurement==0)
            {
                stringstream st;              
                st << "Radar is selected for use but not active - exit"<< endl;
                sendStringStatusErrorToMSDK(st, vehicle);
                return;
            }

        //transform incoming datastring to int
        info = std::strtol(s3String.c_str(), 0, 10);
        reachedHeigthMin = (float)info*0.95;
        reachedHeigthMax = (float)info*1.05;
        // globalPosition = vehicle->broadcast->getGlobalPosition();

        stringstream st1;
        stringstream st2;
        st1  << "Min: " << reachedHeigthMin << " Max: " << reachedHeigthMax << endl;
        st2  << "OSDK Took Control - Craft is going to: " << info << "m"<< endl;
        sendStringStreamStatusToMSDK(st1, vehicle);
        sendStringStreamStatusToMSDK(st2, vehicle);        
        obtainOSDKControl(vehicle);
        gotoHeightStart=1;
        sendStringStartStatusToMSDK(vehicle);
        }
        else         
        {               
                stringstream stRG;              
                stRG << "Vehicle on ground - Exit"<< endl;
                sendStringStatusErrorToMSDK(stRG, vehicle);           
                gotoHeightStart=0;
                sendStringStartStatusToMSDK(vehicle);
            }
  
        }  
    ///////////////////////LIMIT
    else if(Header.compare("LimitDown") ==0)
    {

        //Quit and deactivate Limit if Command is as following
        if(stringData.compare("LimitDown,0,0,") ==0)
        {
            limitHeightStart=0;
            stringstream stRG;              
            stRG << "Limit Down OFF" << endl;
            sendStringStreamStatusToMSDK(stRG, vehicle);
            sendStringStartStatusToMSDK(vehicle);
            releaseOSDKControl(vehicle);
            return;
        }

        //only make limit working if Telemetry or Radar Measurement is on and Vehicle is in the air   
        if(status.flight&&limitFrontStart==0)
        {
        ////transform incoming datastring to int
        limitMinDown = strtol(s3String.c_str(), 0, 10);
        limitMaxDown = strtol(s4String.c_str(), 0, 10);

        stringstream st1;
        stringstream st2;
        st1  << "Flight will be limited from " << limitMinDown << "m to " << limitMaxDown<< "m" << endl;
        st2  << "OSDK will take Control if neccesarry" << endl;
        sendStringStreamStatusToMSDK(st1, vehicle);
        sendStringStreamStatusToMSDK(st2, vehicle);

        limitHeightStart=1;
        sendStringStartStatusToMSDK(vehicle);
        }
        else         
        {                           
                stringstream stR;
                stR << "Vehicle on ground - cant set Limit " << endl;
                sendStringStatusErrorToMSDK(stR, vehicle);
                limitHeightStart=0;
                sendStringStartStatusToMSDK(vehicle);
         }
    }
    else if(Header.compare("LimitFront") ==0)
    {
        //Quit and deactivate Limit if Command is as following
        if(stringData.compare("LimitFront,0,0,") ==0)
        {
            limitFrontStart=0;
            stringstream stRG;              
            stRG << "Limit Front OFF" << endl;
            sendStringStreamStatusToMSDK(stRG, vehicle);           
            sendStringStartStatusToMSDK(vehicle);
            releaseOSDKControl(vehicle);
            return;
        }

        //FrontLimit is only working if FrontRadar is installed, RadarMeasurement is going and vehicle is in the air
        if(startRadarMeasurement && status.flight && RadarFront && limitHeightStart ==0)
        {
            ////transform incoming datastring to int
            limitMinFront = strtol(s3String.c_str(), 0, 10);
            limitMaxFront = strtol(s4String.c_str(), 0, 10);

            stringstream st1;
            stringstream st2;
            st1  << "Flight will be limited from " << limitMinDown << "m to " << limitMaxDown<< "m Distanc" << endl;
            st2  << "OSDK will take Control if neccesarry" << endl;
            sendStringStreamStatusToMSDK(st1, vehicle);
            sendStringStreamStatusToMSDK(st2, vehicle);

            limitFrontStart=1;
            sendStringStartStatusToMSDK(vehicle);
        }
        else         
        {                           
                stringstream stR;
                stR << "Radar n/a or vehicle on ground - cant set Limit " << endl;
                sendStringStatusErrorToMSDK(stR, vehicle);
                sendStringStartStatusToMSDK(vehicle);
                limitFrontStart=0;
         }



    }   
    else if(Header.compare("AppStarted") ==0)
    {
        sendStringStartStatusToMSDK(vehicle);
    }  
    else if(Header.compare("STOPALL") ==0)
    {
        //Set everything OFF and Send to Mobile and EmergencyBrake
        startRadarMeasurement = 0;
        Radar1GPS0Variable    = 0;
        LoggingRadar          = 0;
        LoggingTelemetry      = 0;
        limitHeightStart      = 0;
        limitFrontStart       = 0;
        gotoHeightStart       = 0;

        emergencyBrake_diy(vehicle);
        stringstream st;
        st << "STOP ALL" << endl;
        sendStringStreamStatusToMSDK(st, vehicle);
        sendStringStartStatusToMSDK(vehicle);
        releaseOSDKControl(vehicle);
    }  

    //Send Ack
    stringstream strAck;
    strAck  << "ACK,"<< stringData << endl;
    sendStringStreamToMSDK(strAck, vehicle);
    //cout << "ACK sent: " << dataStringAck << endl;

   

}


void updateTelemetryData(Vehicle* vehicle)
{

        struct radardata r;

        //get Telemetry Data
        batCap          = vehicle->broadcast->getBatteryInfo().voltage;
        globalPosition  = vehicle->broadcast->getGlobalPosition();
        data            = vehicle->broadcast->getGPSInfo();
        status          = vehicle->broadcast->getStatus();
        velocity        = vehicle->broadcast->getVelocity();
        quaternion      = vehicle->broadcast->getQuaternion();
        if(LoggingTelemetry)
        {
            //Writing to File
            stringstream stream_writeTelemetry;
            stream_writeTelemetry << "T,"
                << fixed << r.rf_time << ","
                << status.flight << ","
                << batCap << "," 
                << globalPosition.altitude << "," 
                << globalPosition.height << "," 
            << endl;
            string string_telem = stream_writeTelemetry.str();
        

                
        if (!ofs) 
        { 
            std::cerr<<"Error writing to ..."<<telemetryRecordingFileName<<std::endl; 
            }

        else 
        {
            ofs.open (telemetryRecordingFileName, std::ofstream::out | std::ofstream::app);
            ofs << string_telem; 
            ofs.close(); 
            }  
        }   
}


struct radardata do_rf_measurement(int measurement_count)
{

    struct radardata r;
    float rf_distance;
    signed int rf_angle;
    float rf_angle_deg;
    signed int rf_speed;
    float rf_time;
    signed int value_int = 0;
     
    int i;
    int j;
    //puts("1");
    for (i = 0; i < measurement_count; i++)
    {
      //04 Send Command Do Range Finder Measurement -> 0x68
      tx_msg = 0x68;
      tx_msg_return = send(socket_desc, &tx_msg, sizeof(tx_msg), 0);
      //05 Wait for Approval <- 0x68
      recv(socket_desc, &echo, sizeof(echo), 0);
      //puts("2");
      for (j = 0; j < 5; j++)
      {
          if ((msg_size = read(socket_desc, &value_int, sizeof(value_int))) <= 0)
              break;
          else
          {
              if (j == 0)
              {
                  rf_distance = value_int;
                  rf_distance = rf_distance/1000;
              }
              if (j == 1)
              {
                  rf_angle = value_int;
                  //Calculate Angle in degree
                  //rf_angle_deg = (rf_angle*57.295)/(33554432);
                  rf_angle_deg = (rf_angle * 57.295) / (33554432);
              }
              if (j == 2)
              {
                  rf_speed = value_int;
              }
              if (j == 3)
              {
                  rf_time = value_int;
                  rf_time = rf_time/1000;
              }
              value_int = 0;
          }
      }

      r.rf_distance = rf_distance;
      r.rf_angle_deg = rf_angle_deg;
      r.rf_speed = rf_speed;
      r.rf_time = rf_time;
      // puts("2");
    }
    return r;
   puts("3");
} 

int init_rf_measurement(void)
{
    //Create socket
    socket_desc = socket(AF_INET, SOCK_STREAM, 0);
    printf("SUCCESS: Socket created\n");

    if (socket_desc == -1)
    {
        printf("ERROR: Could not create socket!\n");
        return 0;
    }

    //radar ip is defined above
    //server.sin_addr.s_addr = inet_addr("192.168.0.98");
    server.sin_family = AF_INET;
    server.sin_port = htons(1024);

    //try for bind with own adress
    //Adresse der Netzwerkkarte an der das Radar angeschlossen ist -> muss vorher eingestellt werden!
    //IP: 192.168.0.3, Gate: 192.168.0.1
    //Bind kann genutzt werden um verschiedene Module an verschiedenen Netzwerkkarten zu binden!
    server.sin_addr.s_addr = inet_addr("192.168.0.3");
    puts("Binding to Network Interface...\n");
    if (bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        puts("ERROR: bind error!\n");
        puts("Interface is not connected or already bound\n");
       // return 0;
    }
    else
    {
        puts("Binding OK\n");
    }
    

    // //Connect to remote server
    // LINUX = Client, Radar = Server
    //IP vom Radarmodul ->
    server.sin_addr.s_addr = inet_addr("192.168.0.98");
    puts("Connecting to Radar...\n");
    if (connect(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        puts("ERROR: connect error!\n");
        return 0;
    }

    puts("Connected OK\n");

    //////Comannds to Send
    //01 Send Command  System parameters -> 0x28
    //02 Wait for Approval <- 0x28
    //03 System parameters ->
    //04 Send Command Do Range Finder Measurement -> 0x66
    //05 Wait for Approval <- 0x66
    //06 Wait for Stream of Results <-

    //01 Send Command  System parameters -> 0x28
    tx_msg = 0x28;
    tx_msg_return = send(socket_desc, &tx_msg, sizeof(tx_msg), 0);
    //02 Wait for Approval <- 0x28
    recv(socket_desc, &echo, sizeof(echo), 0);
    ////////03  System parameters -> 0x0404010101010A01002300C000e85d0003:
    //packet_bytes gesniffet via Wireshark
    //Standardsettings
    //   unsigned char packet_bytes[] = {
    //       0x04, 0x01, 0x01, 0x00, 0x01, 0x00, 0x01, 0x0a,
    //       0x00, 0x00, 0x44, 0x00, 0xc0, 0x5d, 0xfa, 0x00, 0x00
    //     };

    //24-25Ghz Bandbreite, 1ms ramptime
    // unsigned char packet_bytes_system[] = 
    // {
    // 0x04, 0x0a, 0x01, 0x01, 0x01, 0x00, 0x01, 0x0a,
    // 0x00, 0x00, 0x23, 0x00, 0xc0, 0x5d, 0xe8, 0x03,
    // 0x00
    // // };
    // unsigned char packet_bytes_system[]  = {
    // 0x04, 0x01, 0x01, 0x01, 0x01, 0x00, 0x01, 0x0a,
    // 0x00, 0x00, 0x01, 0x02, 0xc0, 0x5d, 0xe8, 0x03,
    // 0x00
    // };

    //23500 - 25500 ~7cm resolution
    unsigned char packet_bytes_system[] = {
    0x04, 0x01, 0x01, 0x01, 0x01, 0x00, 0x01, 0x0a,
    0x00, 0x00, 0xfc, 0x01, 0xcc, 0x5b, 0xd0, 0x07,
    0x00
    };


    tx_msg_return = send(socket_desc, (&packet_bytes_system), sizeof(packet_bytes_system), 0);
    puts("System Params Set -");


    ////////04 Send Set Range Finder Params
    tx_msg = 0x66;
    tx_msg_return = send(socket_desc, &tx_msg, sizeof(tx_msg), 0);
    //02 Wait for Approval <- 0x66
    recv(socket_desc, &echo, sizeof(echo), 0);
    //0x66, Max Acc 2000, Threshold -10, Max Uncertainty 2000
    unsigned char packet_bytes_rf[] = 
        {
        0x01, 0x00, 0xd0, 0x07, 0x00, 0x00, 0xf6, 0xff,
        0xd0, 0x07, 0x01, 0x00
        };
    tx_msg_return = send(socket_desc, (&packet_bytes_rf), sizeof(packet_bytes_rf), 0);
    puts("Range Finder Params Set -");

  return 1;
}


void sendStringStreamToMSDK(std::stringstream& strIn, Vehicle* vehicle)
{
    std::string dataString = strIn.str();
    //Convert String to Char Array for Sending
    char char_array[dataString.length()];
    strcpy(char_array, dataString.c_str()); 
    //Sending String to MSDK
    vehicle->moc->sendDataToMSDK(reinterpret_cast<uint8_t*>(&char_array),sizeof(char_array));   
    strIn.str("");
    strIn.clear();
}

void sendStringStreamStatusToMSDK(std::stringstream& strIn, Vehicle* vehicle)
{  

    cout <<  strIn.str();
    stringstream    strInEdit;

    strInEdit << "StatusLine," << strIn.str();
    std::string dataString = strInEdit.str();
    //Convert String to Char Array for Sending
    char char_array[dataString.length()];
    strcpy(char_array, dataString.c_str()); 
    //Sending String to MSDK
    vehicle->moc->sendDataToMSDK(reinterpret_cast<uint8_t*>(&char_array),sizeof(char_array));   
    strIn.str("");
    strIn.clear();
}


void sendStringStartStatusToMSDK(Vehicle* vehicle)
{  
    stringstream sendStream;
    sendStream << "StartStatus,"
        << startRadarMeasurement << ","
        << 1 << "," //Was Sendtelemetry before 
        << Radar1GPS0Variable << "," 
        << LoggingRadar << ","  
        << LoggingTelemetry << "," //5
        << limitHeightStart << "," 
        << limitFrontStart << "," 
        << enableRadar << "," 
        << RadarFront << ","
        << RadarDown << "," //10
    << endl;

    std::string dataString = sendStream.str();
    //Convert String to Char Array for Sending
    char char_array[dataString.length()];
    strcpy(char_array, dataString.c_str()); 
    //Sending String to MSDK
    vehicle->moc->sendDataToMSDK(reinterpret_cast<uint8_t*>(&char_array),sizeof(char_array));   
    sendStream.clear();
}

void sendStringStatusErrorToMSDK(std::stringstream& strIn, Vehicle* vehicle)
{  
    cout <<  strIn.str();
    stringstream    strInEdit;

    strInEdit << "StatusLine,ERROR: " << strIn.str();
    std::string dataString = strInEdit.str();
    //Convert String to Char Array for Sending
    char char_array[dataString.length()];
    strcpy(char_array, dataString.c_str()); 
    //Sending String to MSDK
    vehicle->moc->sendDataToMSDK(reinterpret_cast<uint8_t*>(&char_array),sizeof(char_array));   
    strIn.str("");
    strIn.clear();
}

void obtainOSDKControl(Vehicle* vehicle)
{
    vehicle->obtainCtrlAuthority(functionTimeout);
    OSDKControl=1;
    std:stringstream s;
    s  << "OSDKCtrl,1,"<< endl;
    sendStringStreamToMSDK(s, vehicle);

    stringstream st;              
    st << "OSDK took Control"<< endl;
    sendStringStreamStatusToMSDK(st, vehicle);     

}
void releaseOSDKControl(Vehicle* vehicle)
{
    vehicle->releaseCtrlAuthority(functionTimeout);
    OSDKControl=0;
    std:stringstream s;
    s  << "OSDKCtrl,0,"<< endl;
    sendStringStreamToMSDK(s, vehicle);
    
    stringstream st;              
    st << "OSDK released Control"<< endl;
    sendStringStreamStatusToMSDK(st, vehicle);     
}


void emergencyBrake_diy(Vehicle* vehicle)
{
    vehicle->control->velocityAndYawRateCtrl_Body(0,0,0,0);
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
