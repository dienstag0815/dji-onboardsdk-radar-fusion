/*! @file mobile_sample.cpp
 *  @version 3.3
 *  @date Jun 05 2017
 *
 *  @brief
 *  Mobile SDK Communication API usage in a Linux environment.
 *  Shows example usage of the mobile<-->onboard SDK communication API.
 *
 *  @copyright
 *  2017 DJI. All rights reserved.
 * */

#include "mobile_sample.hpp"
#include <iostream>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <ctime>

using namespace std;
short tx_msg;
unsigned int tx_msg_return;
int msg_size;
short echo;
int socket_desc;
struct sockaddr_in server;

 struct radardata {

    float rf_distance;
    float rf_angle_deg;
    signed int rf_speed;
    float rf_time;
      
    };

using namespace std;
using namespace DJI;
using namespace DJI::OSDK;


//declare functions
void init_rf_measurement(void);
struct radardata do_rf_measurement(int measurement_count);

int
main(int argc, char** argv)
{

  // Setup OSDK.
  Vehicle* vehicle = setupOSDK(argc, argv);
  if (vehicle == NULL)
  {
    std::cout << "Vehicle not initialized, exiting.\n";
    return -1;
  }
  
  struct radardata r;

  //Setup Socket & Radar
  init_rf_measurement();
  puts("- Start Range Finder Measurement - \n ");

 while(1){

    usleep(100*1000);
    //do 1 Measurement in Range Finder Mode and put Data in r struct with rf_distance, rf_angle_deg and rf_speed
    r = do_rf_measurement(1);
    

    if(r.rf_distance > 300)
    {r.rf_distance=303;}
    //Add 100degree to stay positive
    r.rf_angle_deg = r.rf_angle_deg+100;

    //Put String together to Send
    std:stringstream s;
    s << std::fixed << std::setprecision(1) << +r.rf_distance << "," 
        << std::fixed << std::setprecision(1) << +r.rf_angle_deg << ","
        << std::dec << +r.rf_speed << ",,\n";
    std::string dataString = s.str();
    std::cout << dataString;
    //will look like this 6.5,137.5,0,,

    //Convert String to Char Array for Sending
    char char_array[dataString.length()];
      strcpy(char_array, dataString.c_str()); 

      //Sending String to MSDK
      
    vehicle->moc->sendDataToMSDK(reinterpret_cast<uint8_t*>(&char_array),sizeof(char_array));
}
 
  //close socket
  printf("-end ");
  close(socket_desc);
  printf("-socket closed \n");
    
  //Close OSDK
  delete (vehicle);
  return 0;
}

struct radardata do_rf_measurement(int measurement_count)
{
  struct radardata r;
    float rf_distance;
    signed int rf_angle;
    float rf_angle_deg;
    signed int rf_speed;
    float rf_time;
    signed int value_int = 0;
   
   
    int i;
    int j;

    for (i = 0; i < measurement_count; i++)
    {
      //04 Send Command Do Range Finder Measurement -> 0x68
      tx_msg = 0x68;
      tx_msg_return = send(socket_desc, &tx_msg, sizeof(tx_msg), 0);
      //05 Wait for Approval <- 0x68
      recv(socket_desc, &echo, sizeof(echo), 0);
      
      for (j = 0; j < 5; j++)
      {
          if ((msg_size = read(socket_desc, &value_int, sizeof(value_int))) <= 0)
              break;
          else
          {
              if (j == 0)
              {
                  rf_distance = value_int;
                  rf_distance = rf_distance/1000;
              }
              if (j == 1)
              {
                  rf_angle = value_int;
                  //Calculate Angle in degree
                  //rf_angle_deg = (rf_angle*57.295)/(33554432);
                  rf_angle_deg = (rf_angle * 57.295) / (33554432);
              }
              if (j == 2)
              {
                  rf_speed = value_int;
              }
              if (j == 3)
              {
                  rf_time = value_int;
                  rf_time = rf_time/1000;
              }
              value_int = 0;
          }
      }
      //print measurement results
/*       std::cout << "Time:"<< std::fixed << std::setprecision(3) << +rf_time << "s - " 
      << "Distance:" << std::fixed << std::setprecision(3) << +rf_distance << "m - " 
      << "Angle:" << std::fixed << std::setprecision(3) << +rf_angle_deg << "° - " 
      << "Speed:" << std::dec << +rf_speed << "\n"; */

   /*    std::cout << std::fixed << std::setprecision(3) << +rf_distance << "," 
      << std::fixed << std::setprecision(3) << +rf_angle_deg << ","
      << std::dec << +rf_speed << "\n"; */

      r.rf_distance = rf_distance;
      r.rf_angle_deg = rf_angle_deg;
      r.rf_speed = rf_speed;
      r.rf_time = rf_time;
      
    }
    return r;
  }
  


void init_rf_measurement(void)
{
  //Create socket
  socket_desc = socket(AF_INET, SOCK_STREAM, 0);
  printf("- Socket created ");

  if (socket_desc == -1)
  {
      printf("Could not create socket");
  }

  server.sin_addr.s_addr = inet_addr("192.168.0.98");
  server.sin_family = AF_INET;
  server.sin_port = htons(1024);

  //Connect to remote server
  if (connect(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
  {
      puts("connect error\n");
      return;
  }

  puts("- Connected - ");

  //////Comannds to Send
  //01 Send Command Set System parameters -> 0x28
  //02 Wait for Approval <- 0x28d
  //03 Set System parameters -> 0x0404010101010A01002300C000e85d0003:
  //04 Send Command Do Range Finder Measurement -> 0x68
  //05 Wait for Approval <- 0x68
  //06 Wait for Stream of Results <-

  //01 Send Command Set System parameters -> 0x28
  tx_msg = 0x28;
  tx_msg_return = send(socket_desc, &tx_msg, sizeof(tx_msg), 0);

  //02 Wait for Approval <- 0x28

  recv(socket_desc, &echo, sizeof(echo), 0);

  //03 Set System parameters -> 0x0404010101010A01002300C000e85d0003:
  //packet_bytes gesniffet via Wireshark
  unsigned char packet_bytes[] = {
      0x04, 0x01, 0x01, 0x00, 0x01, 0x00, 0x01, 0x0a,
      0x00, 0x00, 0x44, 0x00, 0xc0, 0x5d, 0xfa, 0x00, 0x00
    };
  tx_msg_return = send(socket_desc, (&packet_bytes), sizeof(packet_bytes), 0);
  return;
}